#README#

================================================

Wow... You are actually reading the README file.
You're different. I like you.

================================================

Sergalbot is a custom Discord bot written for the Cedar Lodge.
The bot is written in JavaScript using Node.JS, with ports for it in other languages coming along the way.

## Repository tree: ##
**master** --> Mainline code [JavaScript]

**py35**   --> Python port [Python 3.5]

All branches are licensed with the GNU General Public License version 2 (until further notice), and is available in LICENSE.md.